# Assignment 5: WebGL #

Create a program that allows a scene to be viewed in 3D from any
direction under user control using WebGL.

Time taken to complete assignment: About 8 hours, collectively.

## How to Use ##

Run this guy from a python server: 

	cd <this directory>
	python -m SimpleHTTPServer 8000

...then navigate to localhost:8000 and into model-viewer.html

The checkboxes at the top do two things, but the page must be reloaded
for the change to have any effect:

- Change the shape: If checked, returns a cube shape (what was
  originally in ex08). If unchecked, it returns an isocahedron, the
  building block of a sphere in vertex languages.
- Add a texture: Adds a cute texture from the video game Portal to the
  cube. There is no texture on the isocahedron.

- - - - - -

## Original Assignment ##

	Assignment 5: WebGL

	Create a program that allows a scene to be viewed in 3D from any
	direction under user control using WebGL. At a minumum you need to add
	lighting and textures to Ex 8 and draw a different 3D solid object.

	You may use Apple's CanvasMatrix library of functions provided in
	class or an equivalent library to provide the transformation functions
	needed since I am assuming that you know how to do transformations
	from the previous class.  However, you may NOT use libraries to create
	the objects.  The purpose of this assignment is to give you some
	exposure to how objects are built in OpenGL ES.

	You MAY NOT use the Three.js library or equivalent packages that does
	all the OpenGL calls for you.  The reason is that it abstracts the
	objects and controls to the extent that you never see WebGL.  I want
	you to see how WebGL actually uses OpenGL ES 2.0.

	There are a lot of new things to cope with here (HTML, Javascript and
	OpenGL ES), so I don't expect you to draw a very complex scene, but I
	do want you to make sure you can explain every line of code.

	What to submit:

	1) One ZIP or TAR archive containing all files you are submitting;

	2) README with brief instructions on how to use the program and what I
	should be looking for;

	3) Time it took you to complete the assignment.

	I will view your submission in Chromium 40 or Firefox 35.  You may
	want to test in at least one of those, and in your README indicate if
	there is a reason to use one or the other.

	When running Chromium, use --allow-file-access-from-files to allow
	additional files to be loaded from disk.
