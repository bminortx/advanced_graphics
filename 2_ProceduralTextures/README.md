# Assignment 2: Procedural Textures #

Create a program that allows a scene to be viewed in 3D from any direction under user control.

Time taken to complete assignment: About 4 hours (very little programming, mostly just reading up on shaders)

New files:
- bump.frag : Produces a crystal-like pattern on the object

## How to Use ##

New key bindings
- u/U        : Change the density of the crystal shading
- r/R        : Change the spread of the crystal shading

Unchanged key bindings
- m/M        : Cycle through shaders
- o/O        : Cycle through objects
- p/P        : Toggle between orthogonal & perspective projection
- s/S        : Start/stop light
- X/Y/Z      : Change mandelbrot parameters
- -/+        : Change light elevation
- a          : Toggle axes
- arrows     : Change view angle
- PgDn/PgUp  : Zoom in and out
- 0          : Reset view angle
- ESC        : Exit

- - - - - - 

## Original Assignment ##

	Assignment 2: Procedural Textures

	Create a program that allows a scene to be viewed in 3D from any
	direction under user control.

	The objects in the scene should be colored using a procedural texture
	shader. You may not use the brick or mandlebrot shaders shown in
	class.  If you use a shader from the text or the web, you should
	improve it.

	This is your first real shader and is an opportunity to explore a few
	of the features of GLSL including the builtin functions which provides
	useful manipulations.

	Your scene should be lit.  You may reuse the phong() function from
	model.vert but improvements are always encouraged.

	The resulting shader may be only a few lines long, so make sure you
	can justify and explain every line.

	What to submit:

	1) One ZIP or TAR archive containing all files you are submitting;

	2) Source code (shader and prorgam), Makefile and any data files I
	need;

	3) README with brief instructions on how to use the program;

	4) Time it took you to complete the assignment.
