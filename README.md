# Advanced Graphics Tutorial #

This repo documents my work through Advanced Graphics, a course offered through CU Boulder.

### Assignment Breakdown ###

1. Create a program that displays a scene in 3D that can be viewed from any direction under user control.
2. Procedural Textures
3. Performance Tests
4. Shaders | Transition from OpenGL2 to OpenGL3/4
5. WebGL
6. OpenGL ES (iPhone/Android)
7. 
