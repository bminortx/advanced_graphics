# Assignment 3: Performance #

Investigate the performance of different elements of GLSL.  NOTE: To
turn of VSync for NVIDIA on a per-command basis, prefix the executable
with `vblank_mode=0`:

```vblank_mode=0 ./performance```

Time taken to complete assignment: 2 hours, just thinking of things to
test.

## How to Use ##

Key bindings
- m/M : Cycle through shaders
- o/O : Cycle through objects
- p/P : Toggle between orthogonal & perspective projection
- s/S : Start/stop light
- -/+ : Change light elevation
- a : Toggle axes
- arrows : Change view angle
- PgDn/PgUp : Zoom in and out
- 0 : Reset view angle
- ESC : Exit

## Comparisons ##

I compared using the stretehced-out math to the built-in functions of
OpenGL. The result was surprising (one which you can test for
yourself). Using the stretched-out math instead of the built-in
functions actually slowed down the program by a significant amount
(from 284 -> 245 fps on my machine). What this indicates to me is that
OpenGL's functions are optimized a bit more than my rewritten
functions. For instance, the inverseSqrt function in OpenGL may not
actually use the divisor symbol `/`, which is a slow operation. It
might also do the dot product in parallel... regardless, stretching it
out didn't really do much.

- - - - - -

## Original Assignment ##

	Assignment 3: Performance

	Investigate the performance of different elements of GLSL.  Examples
	of things you may investigate is float vs. int, performance of
	constructs like if, select, builtin functions vs. explicit calculaton,
	the cost of transcendental functions, inline code vs. function calls,
	performance differences between different video cards or operating
	systems, etc.

	What the program should do is left to your imagination.  The scene
	could involve lighting, textures, and similar features, but the
	assignment is to investigate performance of different ways of doing
	things.

	Example 4 contains code to calculate frames per second, which measures
	the overall performance.  Make sure you disable VSYNC so that this
	measures to true frame rate.

	What to submit:

	1) README with discussion of results and brief instructions on how to
	use the program;

	2) One ZIP or TAR archive containing all files you are submitting;

	3) Source code (shader and prorgam), Makefile and any data files I
	need;

	4) Time it took you to complete the assignment.
