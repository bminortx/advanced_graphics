// Custom shader developed by Brandon Minor
varying float LightIntensity;
varying vec2  ModelPos;
varying vec3 ModelXYZ;
varying vec3 LightDir;
varying vec3 EyeDir;
varying vec3 normal;
uniform float time;

// Crystal, unrolled!
// Modified from the OpenGL Programming Guide 8ed, ex 8.10

uniform float CrystalDensity;
uniform float CrystalSize;

void main() {
  vec4 surface_color = vec4(0.0, 0.5, 0.5, 1.0);
  vec2 center = CrystalDensity * ModelPos;
  vec2 perturb = fract(center) - vec2(0.5);
  // Unroll dot
  float dist = (LightDir.x * normal.x +
                LightDir.y * normal.y +
                LightDir.z * normal.z);
  // unroll invsqrt
  float norm_factor = 1.0 / pow((dist + 1.0), 2.0);
  if (dist <= CrystalSize) {
    perturb = vec2(0.0);
    norm_factor = 1.0;
    surface_color.g = 0.1;
  }
  vec3 norm_delta = vec3(perturb.x, perturb.y, 1.0) * norm_factor;
  vec3 color = surface_color.rgb * max(
      (norm_delta.x * LightDir.x +
       norm_delta.y * LightDir.y +
       norm_delta.z * LightDir.z), 0.1);
  // Unroll reflect (and dot)
  vec3 reflect_dir = LightDir - 2.0 * (norm_delta.x * LightDir.x +
                                       norm_delta.y * LightDir.y +
                                       norm_delta.z * LightDir.z) * norm_delta;
  float spec = max((EyeDir.x * reflect_dir.x +
                    EyeDir.y * reflect_dir.y +
                    EyeDir.z * reflect_dir.z), 0.0);
  color = min(color, vec3(1.0));
  gl_FragColor = vec4(color, surface_color.a);
}
