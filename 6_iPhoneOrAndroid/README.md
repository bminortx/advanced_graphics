# Assignment 6: iPhone or Android #

Create a program that allows a scene to be viewed in 3D from any
direction under user control that can be run on the iPhone/iPod/iPad
or Android.

Time taken to complete assignment: 3 days, abouts. Since I was already
familiar with Android programming, most of that time dealt with
figuring out OpenGL ES, and creating/testing my PLY loader.

## How to Use ##

This project is written with Gradle support, and does not use
JNI. This should make it a breeze to compile in Android Studio. The
complete .apk is also located in the top directory (where this README
is), so you can just install directly onto your phone using adb. There
is only one .ply located in memory, but the parser is not unique to
this file; the only requirement is that it be in ASCII format.

- Drag to rotate the object around the screen.
- Move two fingers towards/away each other for a scaling effect. It's
  recommended that your first finger stay in one place on the screen,
  to avoid simultaneous rotation of the object. 

And that's it! Since there are so many vertices, it takes a couple of
seconds for anything to show on the screen (~7 sec on my HTC One
M8). There are myriad improvements to be made to the app, but it's
complete enough for a proof-of-concept.

- - - - - -

## Original Assignment ##

	Assignment 6: iPhone or Android

	Create a program that allows a scene to be viewed in 3D from any
    direction under user control that can be run on the
    iPhone/iPod/iPad or Android.

	You must build some solid 3D objects yourself using OpenGL ES.  In
	particular, there are a number of frameworks such as GLKit on the
	iPhone that permits you to write applications without using OpenGL ES.
	In this assignment I would like you to get some exposure to the nuts
	and bolts of OpenGL ES, so using such frameworks is prohibited.

	You may want to start with my simple application and explore features
	such as lighting and textures, gestures, and so on.  However, you do
	NOT have to follow my example (borrowed from Rideout's iPone 3D
	programming) and develop the core code in C++ and only providing a
	native wrapper.  You may want to stick with Objective C on the iPhone
	or Java on the Android throughout your program. The Android SDK
	contains examples that are pure Java.

	The requirements are deliberately vague.  What I want you to do is
	improve significantly upon what you start with.  So your readme show
	itemize the improvements you made from whatever you tarted with.

	This is potentially a difficult assignment since you need to install
	the Xcode environment for the iPhone or the Android SDK/NDK.
	Therefore you may work in pairs for this assignment with somebody a
	bit further along on the curve.  Your README should clearly tell me
	what the contribution of each team member was.

	When you submit the assignment, the person whose environment you used
	should upload the project.  Look at my examples on how to clean out
	stuff I don't need, and upload only that.

	For the presentations, the presentation would be by one individual.

	If the iPhone/Android is your thing, please volunteer.  However,
	please bear in mind that many people in class are just starting, so be
	careful not to talk over people's heads.

	What to submit:

	1) One ZIP or TAR archive containing all files you are submitting;
	Please submit just one per team.

	2) README with brief instructions on how to use the program and what I
	should be looking for.  The README should be clear on who the team
	members are and what each team member contributed to the assignment,
	and what improvements you made to whatever you started with.

	3) Time it took you to complete the assignment.
