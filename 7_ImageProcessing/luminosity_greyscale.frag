/* Greyscale Luminosity Image
 */

uniform float red;
uniform float green;
uniform float blue;
uniform float threshold;
uniform float dX;
uniform float dY;
uniform sampler2D img;

float sample(float dx, float dy) {
  vec4 orig_color = texture2D(img,gl_TexCoord[0].st+vec2(dx,dy));
  float grey = dot(orig_color.xyz, vec3(red, green, blue));
  return grey;
}

void main() {
  float left_top = sample(-dX, 0.0);
  float center_top = sample(0.0, 0.0);
  float right_top = sample(dX, 0.0);
  float left_bottom = sample(-dX, dY);
  float center_bottom = sample(0.0, dY);
  float right_bottom = sample(dX, dY);
  float SSD = pow(center_top - left_top, 2) +
              pow(right_top - center_top, 2) +
              pow(center_bottom - left_bottom, 2) +
              pow(right_bottom - center_bottom, 2);
  if (SSD > threshold) {
    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
  } else {
    gl_FragColor = vec4(center_top, center_top, center_top, 1.0);
  }

}
