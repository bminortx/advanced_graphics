# Assignment 7: Image Processing #

Create a program that performs image processing using GLSL.

Time taken to complete assignment: 3 hours. 

## How to Use ##

This program transforms the current image into a greyscale image, then detects
corners within the image using a very parsed-down SSD comparison. Based on Ex12.

New Key bindings:

- g         : Change the type of greyscale conversion
- </>       : Raise or lower the corner detection threshold

Old Key Bindings:

- m/M       : Cycle through shaders (filters)
- n/N       : Increase/decrease number of passes
- o/O       : Cycle through objects
- p/P       : Toggle between orthogonal & perspective projection
- l/L       : Toggle lighting on/off
- s/S       : Start/stop light
- -/+       : Change light elevation
- a         : Toggle axes
- arrows    : Change view angle
- PgDn/PgUp : Zoom in and out
- 0         : Reset view angle
- ESC       : Exit

- - - - - -

## Original Assignment ##

	Assignment 7: Image Processing
	Create a program that performs image processing using GLSL.

	This is a very domain specific assignment, but the applications are very wide
	ranging.  Some examples are

	1)  Spatial transformations such as sharpening, blurring, edge detection, etc;
	2)  Color transformations;
	3)  Resampling transformations such as averaging, anti-aliasing;
	4)  Multi-image operations;
	5)  Special effects.

	Anybody who can write a shader that will sharpen an image so you can read the
	license plate from the reflection on somebody's sunglasses gets an automatic A+
	in this class.

	Hint:  If you need to write to a buffer that is a different size than the
	window, Example 12 shows how to create a framebuffer object.

	What to submit:

	1) README with discussion of results and brief instructions on how to use the program;

	2) One ZIP or TAR archive containing all files you are submitting;

	3) Source code (shader and prorgam), Makefile and any data files I need;

	4) Time it took you to complete the assignment.
