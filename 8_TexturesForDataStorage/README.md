# Assignment 8: Textures for Data Storage #

Create a program using GLSL that uses textures for data storage.

Time taken to complete assignment: A day, just thinking about what would be good to do. 

## How to Use ##

This is based off of the demo, so all of the usual keybindings apply. The only new ones are presented below:

- u/U : Decrease/Increase phong shading disortion. 

- - - - - -

## Original Assignment ##

	Assignment 8: Textures for Data Storage

	Create a program using GLSL that uses textures for data storage.

	This is a free form assignment.  You can use textures to store
	functional relations, translations, noise, bump maps, or any
	similar application.

	What to submit:

	1) README with discussion of results, brief instructions on how to
	use the program and a short discussion of what specific
	application you are using the texture(s) for;

	2) One ZIP or TAR archive containing all files you are submitting;

	3) Source code (shader and prorgam), Makefile and any data files I
    need;

	4) Time it took you to complete the assignment.
