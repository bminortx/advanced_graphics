/*
 * Getting the texture change from noise.vert
 */

uniform int       mode;
uniform float     time;
uniform int       mod_factor;
// Lena photo
uniform sampler2D LenaTex;

void main() {
  gl_FragColor = texture2D(LenaTex, gl_TexCoord[0].st + gl_TexCoord[1].yw);
}
