/*
 *  Types of Noise
 *
 *  Demonstrate perlin and simplex noise in 2D, 3D and 4D.
 *  The noise is calulated in the shaders.
 *
 *  Key bindings:
 *  m/M        Cycle through shaders (types of noise)
 *  o/O        Cycle through objects
 *  s/S        Start/stop light
 *  p/P        Toggle between orthogonal & perspective projection
 *  -/+        Change light elevation
 *  a          Toggle axes
 *  arrows     Change view angle
 *  PgDn/PgUp  Zoom in and out
 *  0          Reset view angle
 *  ESC        Exit
 */
#include "CSCIx239.h"
int axes=1;       //  Display axes
int mode=0;       //  Shader mode
int move=1;       //  Move light
int roll=1;       //  Rolling brick texture
int proj=1;       //  Projection type
int obj=0;        //  Object
int th=0;         //  Azimuth of view angle
int ph=0;         //  Elevation of view angle
int fov=55;       //  Field of view (for perspective)
double asp=1;     //  Aspect ratio
double dim=3.0;   //  Size of world
int zh=90;        //  Light azimuth
float Ylight=2;   //  Light elevation
int model;        //  Object model
int lena;
int dX, dY;
#define MODE 2
int shader; //  Shader programs
char* text[] = {"Lena Phong Distortion", "No Shader"};
float Emission[]  = {0.0,0.0,0.0,1.0};
float Ambient[]   = {0.3,0.3,0.3,1.0};
float Diffuse[]   = {1.0,1.0,1.0,1.0};
float Specular[]  = {1.0,1.0,1.0,1.0};
float Shinyness[] = {16};


/*
 * SimplexTexture
 *   Create and load a 1D texture for a simplex traversal order lookup table.
 *   This is used for simplex noise only, and only for 3D and 4D noise where there are more than 2 simplices.
 *   (3D simplex noise has 6 cases to sort out, 4D simplex noise has 24 cases.)
 */
int SimplexTexture(int unit)
{
   unsigned int id;
   // This is a look-up table to speed up the decision on which
   // simplex we are in inside a cube or hypercube "cell"
   // for 3D and 4D simplex noise.  It is used to avoid complicated nested
   // conditionals in the GLSL code.
   // The table is indexed in GLSL with the results of six pair-wise
   // comparisons beween the components of the
   // P=(x,y,z,w) coordinates within a hypercube cell.
   //   c1 = x>=y ? 32 : 0;
   //   c2 = x>=z ? 16 : 0;
   //   c3 = y>=z ? 8 : 0;
   //   c4 = x>=w ? 4 : 0;
   //   c5 = y>=w ? 2 : 0;
   //   c6 = z>=w ? 1 : 0;
   //   offsets = simplex[c1+c2+c3+c4+c5+c6];
   //   o1 = step(160,offsets);
   //   o2 = step(96,offsets);
   //   o3 = step(32,offsets);
   //   (For the 3D case, c4, c5, c6 and o3 are not needed.)
   unsigned char simplex4[][4] = {{0,64,128,192},{0,64,192,128},{0,0,0,0},{0,128,192,64},{0,0,0,0},{0,0,0,0},{0,0,0,0},{64,128,192,0},{0,128,64,192},{0,0,0,0},{0,192,64,128},{0,192,128,64},{0,0,0,0},{0,0,0,0},{0,0,0,0},{64,192,128,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{64,128,0,192},{0,0,0,0},{64,192,0,128},{0,0,0,0},{0,0,0,0},{0,0,0,0},{128,192,0,64},{128,192,64,0},{64,0,128,192},{64,0,192,128},{0,0,0,0},{0,0,0,0},{0,0,0,0},{128,0,192,64},{0,0,0,0},{128,64,192,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{128,0,64,192},{0,0,0,0},{0,0,0,0},{0,0,0,0},{192,0,64,128},{192,0,128,64},{0,0,0,0},{192,64,128,0},{128,64,0,192},{0,0,0,0},{0,0,0,0},{0,0,0,0},{192,64,0,128},{0,0,0,0},{192,128,0,64},{192,128,64,0}};

   //  Select texture unit
   glActiveTexture(unit);

   //  Generate 1D texture id and make current
   glGenTextures(1,&id);
   glBindTexture(GL_TEXTURE_1D,id);

   //  Set texture
   glTexImage1D(GL_TEXTURE_1D,0,GL_RGBA,64,0,GL_RGBA,GL_UNSIGNED_BYTE,simplex4);
   glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
   glTexParameteri(GL_TEXTURE_1D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

   // Switch active texture unit back to 0 again
   glActiveTexture(GL_TEXTURE0);

   return id;
}

/*
 * PermTexture(GLuint *texID)
 *    Create and load a 2D texture for a combined index permutation and gradient lookup table.
 *    This texture is used for 2D and 3D noise, both classic and simplex.
 */
int PermTexture(int unit)
{
   unsigned int id;
   int i,j,k=0;
   unsigned char pixels[4*256*256];
   // These are Ken Perlin's proposed gradients for 3D noise. I kept them for
   // better consistency with the reference implementation, but there is really
   // no need to pad this to 16 gradients for this particular implementation.
   // If only the "proper" first 12 gradients are used, they can be extracted
   // from the grad4[][] array: grad3[i][j] == grad4[i*2][j], 0<=i<=11, j=0,1,2
   // (12 cube edges + 4 more to make 16)
   int perm[256]= {151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,190,6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168,68,175,74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,187,208,89,18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186,3,64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,112,104,218,246,97,228,251,34,242,193,238,210,144,12,191,179,162,241,81,51,145,235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,50,45,127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180};
   int grad3[16][3] = {{0,1,1},{0,1,-1},{0,-1,1},{0,-1,-1},{1,0,1},{1,0,-1},{-1,0,1},{-1,0,-1},{1,1,0},{1,-1,0},{-1,1,0},{-1,-1,0},{1,0,-1},{-1,0,-1},{0,-1,1},{0,1,1}};

   //  Select texture unit
   glActiveTexture(unit);

   //  Generate 2D texture id and make current
   glGenTextures(1,&id);
   glBindTexture(GL_TEXTURE_2D,id);

   //  Set pixels
   for (i=0;i<256;i++)
      for(j=0;j<256;j++)
      {
         char value = perm[(j+perm[i]) & 0xFF];
         pixels[k++] = grad3[value & 0x0F][0] * 64 + 64; // Gradient x
         pixels[k++] = grad3[value & 0x0F][1] * 64 + 64; // Gradient y
         pixels[k++] = grad3[value & 0x0F][2] * 64 + 64; // Gradient z
         pixels[k++] = value;                            // Permuted index
      }

   //  Set texture
   glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,256,256,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
   glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

   // Switch active texture unit back to 0 again
   glActiveTexture(GL_TEXTURE0);

   return id;
}

/*
 * GradTexture:
 *   Create and load a 2D texture for a 4D gradient lookup table.
 *   This is used for 4D noise only.
 */
int GradTexture(int unit)
{
   unsigned int id;
   int i,j,k=0;
   unsigned char pixels[4*256*256];
   // These are Stefan Gustavson's gradients for 4D noise.
   // They are the coordinates of the midpoints of each of the 32 edges of a tesseract,
   // just like the 3D noise gradients are the midpoints of the 12 edges of a cube.
   int perm[256]= {151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,190,6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168,68,175,74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,187,208,89,18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186,3,64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,153,101,155,167,43,172,9,129,22,39,253,19,98,108,110,79,113,224,232,178,185,112,104,218,246,97,228,251,34,242,193,238,210,144,12,191,179,162,241,81,51,145,235,249,14,239,107,49,192,214,31,181,199,106,157,184,84,204,176,115,121,50,45,127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180};
   int grad4[32][4]= {{0,1,1,1},{0,1,1,-1},{0,1,-1,1},{0,1,-1,-1},{0,-1,1,1},{0,-1,1,-1},{0,-1,-1,1},{0,-1,-1,-1},{1,0,1,1},{1,0,1,-1},{1,0,-1,1},{1,0,-1,-1},{-1,0,1,1},{-1,0,1,-1},{-1,0,-1,1},{-1,0,-1,-1},{1,1,0,1},{1,1,0,-1},{1,-1,0,1},{1,-1,0,-1},{-1,1,0,1},{-1,1,0,-1},{-1,-1,0,1},{-1,-1,0,-1},{1,1,1,0},{1,1,-1,0},{1,-1,1,0},{1,-1,-1,0},{-1,1,1,0},{-1,1,-1,0},{-1,-1,1,0},{-1,-1,-1,0}};

   //  Select texture unit
   glActiveTexture(unit);

   //  Generate 2D texture id and make current
   glGenTextures(1,&id);
   glBindTexture(GL_TEXTURE_2D,id);

   //  Set pixels
   for (i=0;i<256;i++)
      for (j=0;j<256;j++)
      {
         char value = perm[(j+perm[i]) & 0xFF];
         pixels[k++] = grad4[value & 0x1F][0] * 64 + 64; // Gradient x
         pixels[k++] = grad4[value & 0x1F][1] * 64 + 64; // Gradient y
         pixels[k++] = grad4[value & 0x1F][2] * 64 + 64; // Gradient z
         pixels[k++] = grad4[value & 0x1F][3] * 64 + 64; // Gradient z
      }

   //  Set texture
   glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,256,256,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
   glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

   // Switch active texture unit back to 0 again
   glActiveTexture(GL_TEXTURE0);
   return id;
}

int LenaTexture(int unit) {
  //  Select texture unit
  glActiveTexture(unit);
  lena = LoadTexBMP("lena_color_512.bmp");
  glBindTexture(GL_TEXTURE_2D,lena);
  // Switch active texture unit back to 0 again
  glActiveTexture(GL_TEXTURE0);
  return lena;
}


/*
 *  Draw a cube
 */
static void Cube(void)
{
   //  Front
   glColor3f(1,0,0);
   glBegin(GL_QUADS);
   glNormal3f( 0, 0,+1);
   glTexCoord2f(0,0); glVertex3f(-1,-1,+1);
   glTexCoord2f(1,0); glVertex3f(+1,-1,+1);
   glTexCoord2f(1,1); glVertex3f(+1,+1,+1);
   glTexCoord2f(0,1); glVertex3f(-1,+1,+1);
   glEnd();
   //  Back
   glColor3f(0,0,1);
   glBegin(GL_QUADS);
   glNormal3f( 0, 0,-1);
   glTexCoord2f(0,0); glVertex3f(+1,-1,-1);
   glTexCoord2f(1,0); glVertex3f(-1,-1,-1);
   glTexCoord2f(1,1); glVertex3f(-1,+1,-1);
   glTexCoord2f(0,1); glVertex3f(+1,+1,-1);
   glEnd();
   //  Right
   glColor3f(1,1,0);
   glBegin(GL_QUADS);
   glNormal3f(+1, 0, 0);
   glTexCoord2f(0,0); glVertex3f(+1,-1,+1);
   glTexCoord2f(1,0); glVertex3f(+1,-1,-1);
   glTexCoord2f(1,1); glVertex3f(+1,+1,-1);
   glTexCoord2f(0,1); glVertex3f(+1,+1,+1);
   glEnd();
   //  Left
   glColor3f(0,1,0);
   glBegin(GL_QUADS);
   glNormal3f(-1, 0, 0);
   glTexCoord2f(0,0); glVertex3f(-1,-1,-1);
   glTexCoord2f(1,0); glVertex3f(-1,-1,+1);
   glTexCoord2f(1,1); glVertex3f(-1,+1,+1);
   glTexCoord2f(0,1); glVertex3f(-1,+1,-1);
   glEnd();
   //  Top
   glColor3f(0,1,1);
   glBegin(GL_QUADS);
   glNormal3f( 0,+1, 0);
   glTexCoord2f(0,0); glVertex3f(-1,+1,+1);
   glTexCoord2f(1,0); glVertex3f(+1,+1,+1);
   glTexCoord2f(1,1); glVertex3f(+1,+1,-1);
   glTexCoord2f(0,1); glVertex3f(-1,+1,-1);
   glEnd();
   //  Bottom
   glColor3f(1,0,1);
   glBegin(GL_QUADS);
   glNormal3f( 0,-1, 0);
   glTexCoord2f(0,0); glVertex3f(-1,-1,-1);
   glTexCoord2f(1,0); glVertex3f(+1,-1,-1);
   glTexCoord2f(1,1); glVertex3f(+1,-1,+1);
   glTexCoord2f(0,1); glVertex3f(-1,-1,+1);
   glEnd();
}

/*
 *  OpenGL (GLUT) calls this routine to display the scene
 */
void display()
{
   const double len=2.0;  //  Length of axes
   /* //  Light position and colors */
   float Position[]  = {2*Cos(zh),Ylight,2*Sin(zh),1.0};

   //  Erase the window and the depth buffer
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

   //  Enable Z-buffering in OpenGL
   glEnable(GL_DEPTH_TEST);
   //  Undo previous transformations
   glLoadIdentity();
   //  Perspective - set eye position
   if (proj)
   {
      double Ex = -2*dim*Sin(th)*Cos(ph);
      double Ey = +2*dim        *Sin(ph);
      double Ez = +2*dim*Cos(th)*Cos(ph);
      gluLookAt(Ex,Ey,Ez , 0,0,0 , 0,Cos(ph),0);
   }
   //  Orthogonal - set world orientation
   else
   {
      glRotatef(ph,1,0,0);
      glRotatef(th,0,1,0);
   }

   //  Draw light position as sphere (still no lighting here)
   glColor3f(1,1,1);
   glPushMatrix();
   glTranslated(Position[0],Position[1],Position[2]);
   glutSolidSphere(0.03,10,10);
   glPopMatrix();
   //  OpenGL should normalize normal vectors
   glEnable(GL_NORMALIZE);
   //  Enable lighting
   glEnable(GL_LIGHTING);
   //  glColor sets ambient and diffuse color materials
   glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
   glEnable(GL_COLOR_MATERIAL);
   //  Enable light 0
   glEnable(GL_LIGHT0);
   //  Set ambient, diffuse, specular components and position of light 0
   glLightfv(GL_LIGHT0,GL_AMBIENT ,Ambient);
   glLightfv(GL_LIGHT0,GL_DIFFUSE ,Diffuse);
   glLightfv(GL_LIGHT0,GL_SPECULAR,Specular);
   glLightfv(GL_LIGHT0,GL_POSITION,Position);
   //  Set materials
   glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,Shinyness);
   glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,Specular);
   glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,Emission);

   //
   //  Draw scene
   //
   if (mode==0)
   {
      int id;
      float time = roll ? 0.001*glutGet(GLUT_ELAPSED_TIME) : 0;
      glUseProgram(shader);
      id = glGetUniformLocation(shader,"mode");
      if (id>=0) glUniform1i(id,mode);
      id = glGetUniformLocation(shader,"time");
      if (id>=0) glUniform1f(id,time);
      id = glGetUniformLocation(shader,"LenaTex");
      if (id>=0) glUniform1i(id,lena);
   }

   //  Draw the model, teapot or cube
   glColor3f(1,1,0);
   if (obj==2)
      glCallList(model);
   else if (obj==1)
      glutSolidTeapot(1.0);
   else
      Cube();

   //  No shader for what follows
   glUseProgram(0);

   //  Draw axes - no lighting from here on
   glDisable(GL_LIGHTING);
   glColor3f(1,1,1);
   if (axes)
   {
      glBegin(GL_LINES);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(len,0.0,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,len,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,0.0,len);
      glEnd();
      //  Label axes
      glRasterPos3d(len,0.0,0.0);
      Print("X");
      glRasterPos3d(0.0,len,0.0);
      Print("Y");
      glRasterPos3d(0.0,0.0,len);
      Print("Z");
   }
   //  Display parameters
   glWindowPos2i(5,5);
   Print("FPS=%d  Dim=%.1f Projection=%s Mode=%s",
     FramesPerSecond(),dim,proj?"Perpective":"Orthogonal",text[mode]);
   //  Render the scene and make it visible
   ErrCheck("display");
   glFlush();
   glutSwapBuffers();
}

/*
 *  GLUT calls this routine when the window is resized
 */
void idle()
{
   //  Elapsed time in seconds
   double t = glutGet(GLUT_ELAPSED_TIME)/1000.0;
   if (move) zh = fmod(90*t,360.0);
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when an arrow key is pressed
 */
void special(int key,int x,int y)
{
   //  Right arrow key - increase angle by 5 degrees
   if (key == GLUT_KEY_RIGHT)
      th += 5;
   //  Left arrow key - decrease angle by 5 degrees
   else if (key == GLUT_KEY_LEFT)
      th -= 5;
   //  Up arrow key - increase elevation by 5 degrees
   else if (key == GLUT_KEY_UP)
      ph += 5;
   //  Down arrow key - decrease elevation by 5 degrees
   else if (key == GLUT_KEY_DOWN)
      ph -= 5;
   //  PageUp key - increase dim
   else if (key == GLUT_KEY_PAGE_DOWN)
      dim += 0.1;
   //  PageDown key - decrease dim
   else if (key == GLUT_KEY_PAGE_UP && dim>1)
      dim -= 0.1;
   //  Keep angles to +/-360 degrees
   th %= 360;
   ph %= 360;
   //  Update projection
   Project(proj?fov:0,asp,dim);
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when a key is pressed
 */
void key(unsigned char ch,int x,int y)
{
   //  Exit on ESC
   if (ch == 27)
      exit(0);
   //  Reset view angle
   else if (ch == '0')
      th = ph = 0;
   //  Toggle axes
   else if (ch == 'a' || ch == 'A')
      axes = 1-axes;
   //  Toggle projection type
   else if (ch == 'p' || ch == 'P')
      proj = 1-proj;
   //  Toggle light movement
   else if (ch == 's' || ch == 'S')
      move = 1-move;
   //  Toggle brick movement
   else if (ch == 'b' || ch == 'B')
      roll = 1-roll;
   //  Toggle objects
   else if (ch == 'o' || ch == 'O')
      obj = (obj+1)%3;
   //  Cycle modes
   else if (ch == 'm')
      mode = (mode+1)%MODE;
   else if (ch == 'M')
      mode = (mode+MODE-1)%MODE;
   //  Light elevation
   else if (ch == '+')
      Ylight += 0.1;
   else if (ch == '-')
      Ylight -= 0.1;
   //  Light position
   else if (ch == '[')
      zh--;
   else if (ch == ']')
      zh++;
   /* Increase/decrease phong distortion */
   else if (ch == 'u') {
     for (int i = 0; i < 4; i++) {
       Diffuse[i] /= 1.1;
       Specular[i] /= 1.1;
       /* float Shinyness[] = {16}; */
     }
   } else if (ch == 'U') {
     for (int i = 0; i < 4; i++) {
       Diffuse[i] *= 1.1;
       Specular[i] *= 1.1;
     }
   }

   //  Reproject
   Project(proj?fov:0,asp,dim);
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when the window is resized
 */
void reshape(int width,int height)
{
   //  Ratio of the width to the height of the window
   asp = (height>0) ? (double)width/height : 1;

   //  Set texture offsets for kernel
   dX = 1.0/width;
   dY = 1.0/height;

   //  Set the viewport to the entire window
   glViewport(0,0, width,height);
   //  Set projection
   Project(proj?fov:0,asp,dim);
}

/*
 *  Start up GLUT and tell it what to do
 */
int main(int argc,char* argv[])
{
   //  Initialize GLUT
   glutInit(&argc,argv);
   //  Request double buffered, true color window with Z buffering at 600x600
   glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
   glutInitWindowSize(600,600);
   glutCreateWindow("Types of Noise");
#ifdef USEGLEW
   //  Initialize GLEW
   if (glewInit()!=GLEW_OK) Fatal("Error initializing GLEW\n");
   if (!GLEW_VERSION_2_0) Fatal("OpenGL 2.0 not supported\n");
#endif
   //  Set callbacks
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutSpecialFunc(special);
   glutKeyboardFunc(key);
   glutIdleFunc(idle);
   //  Load object
   model = LoadOBJ("bunny.obj");
   //  Load random texture
   SimplexTexture(GL_TEXTURE1);
   PermTexture(GL_TEXTURE2);
   LenaTexture(GL_TEXTURE3);
   //  Create Shader Programs
   shader = CreateShaderProg("noise.vert","phong_distort.frag");
   //  Pass control to GLUT so it can interact with the user
   ErrCheck("init");
   glutMainLoop();
   return 0;
}
