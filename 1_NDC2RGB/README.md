# Assignment 1: NDC to RGB shader #

Create a program that displays a scene in 3D that can be viewed from any direction under user control.

Time taken to complete assignment: 1 hour

## How to Use ##

Identical to the demo, except the shader in this assignment adjusts the color of the vertex to its NDC. This implementation can be seen in `basic.frag`.

Key bindings: 
-  m/M       : Cycle through shaders
-  o/O       : Cycle through objects
-  p/P       : Toggle between orthogonal & perspective projection
-  a         : Toggle axes
-  arrows    : Change view angle
-  PgDn/PgUp : Zoom in and out
-  0         : Reset view angle
-  ESC       : Exit

- - - - - - 

## Original Assignment ##

	The color of every vertex in every object should be set using a shader
	such that the coordinates of the vertex in Normalized Device
	Coordinates (-1 to +1 in x,y,z) determines the color of the vertex.
	The shader must work correctly with both orthogonal and perspective
	projections, which means you cannot assume w=1.

	This is a trivial shader, and the vertex and fragment shader should
	each be only one or two lines.  So make an effort of making this as
	elegant as you can.  Consider this Shader Golf.

	What to submit:

	1) One ZIP or TAR archive containing all files you are submitting;

	2) Source code, Makefile and any data files I need;

	3) README with brief instructions on how to use the program;

	4) Time it took you to complete the assignment.


