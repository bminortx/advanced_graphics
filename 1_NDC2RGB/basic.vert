void main() {
  //  Set vertex coordinates
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
  gl_FrontColor = ((gl_Position / gl_Position.w) + 1.0) / 2.0;
}
