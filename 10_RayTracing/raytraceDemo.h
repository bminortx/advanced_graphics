/*
 *  Ray Trace Examples
 *
 *  Ray trace three spheres.
 *  Computations are accelerated using OpenMP.
 *
 *  Key bindings:
 *  +/-        Increase/decrease distance between spheres
 *  n/N        Increase/decrease maximum number of reflections
 *  []         Move light
 *  a          Toggle axes
 *  arrows     Change view angle
 *  PgDn/PgUp  Zoom in and out
 *  0          Reset view angle
 *  ESC        Exit
 */

using namespace std;

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <vector>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "Ray.h"
#include "RaySphere.h"
#include <cuda.h>
#include <cuda_runtime_api.h>

#define LEN 8192          //  Maximum length of text string

//  Global variables
int th=0;                 //  Azimuth of view angle
int ph=0;                 //  Elevation of view angle
int wid,hgt;              //  Screen dimensions
int maxlev=8;             //  Maximum levels
char* pixels=NULL;        //  Pixel array for entire screen
char* device_pixels=NULL; //  Pixel array for CUDA kernel
float dim=1;              //  Sphere spacing
float zoom=1;             //  Zoom level
int ray_samples = 1;      //  Anti-alias samples
Mat3 rot;                 //  Rotation matrix
static const int numLight = 2;
static const int numObject = 3;
Light lights[numLight];
Object objects[numObject];
Light dev_lights[numLight];
Object dev_objects[numObject];

__global__
void RaycastGPU(char* output_image,
                Object* objects, int numObjects,
                Light* lights, int numLights,
                int ray_samples, float zoom, int wid,
                int hgt, int maxlev);
void Print(const char* format , ...);
Color RayTrace(Vec3 org,Vec3 dir);
void RayTracePixel(int k);
void display();
void SetRot(void);
void special(int key,int x,int y);
void key(unsigned char ch,int x,int y);
void reshape(int width,int height);
