# Assignment 10: GPU Computing #

Create a program that uses CUDA or OpenCL to speed up calculations.

## How to Use ##


- - - - - -

## Original Assignment ##

	Create a program that uses CUDA or OpenCL to speed up
	calculations.  Compare the speed of a CPU based implementation
	with a GPU implementation.

	It is NOT acceptable to simply take examples 24 or 25 and adding
	meaningless computations to the matrix operations.  You need to
	attempt a meaningful computation using the GPU.  In order to see a
	benefit from the GPU computing, you will likely have to operate on
	a large data set.

	What to submit:

	1) README with discussion of results, brief instructions on how to
    use the program and a short discussion of the parallel algorithm;

	2) One ZIP or TAR archive containing all files you are submitting;

	3) Source code, Makefile and any data files I need;

	4) Time it took you to complete the assignment.
