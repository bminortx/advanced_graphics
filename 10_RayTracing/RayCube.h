#include "RayObject.h"

class Cube : public Object {
 public:
  float size;
  //  Constructor
  Cube(Vec3 p,float s,Material m):
      Object(p,m)
  {
    size = s;
  }
  //  Intersection
  bool hit(Ray& r);
  Vec3 normal(Vec3& p);

 private:
}
