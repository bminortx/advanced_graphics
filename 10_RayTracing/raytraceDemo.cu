#include "raytraceDemo.h"

__global__
void RaycastGPU(char* output_image,
                Object* objects, int numObjects,
                Light* lights, int numLights,
                int ray_samples, float zoom, int wid,
                int hgt, int maxlev) {
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  int j = blockIdx.y * blockDim.y + threadIdx.y;
  // Bounds check
  int index = j * wid + i;
  // char* pix = output_image + 4 * index;
  output_image[4 * index] = 100;
  output_image[4 * index + 1] = 30;
  output_image[4 * index + 1] = 20;
  output_image[4 * index + 1] = 100;
  // *pix++ = static_cast<char>(200);
  // *pix++ = static_cast<char>(40);
  // *pix++ = static_cast<char>(80);
  // *pix++ = static_cast<char>(255);
  if (index > wid * hgt * 4) return;
  Color avg_color(0, 0, 0);

  for (int m=0; m < 2; m++) {
    for (int n=0; n < 2; n++) {
      // Cast the intial ray from (x,y,-1000) down z axis
      Vec3 org(zoom*(i-wid/2 + (n * 0.5) / ray_samples),
               zoom*(j-hgt/2 + (m * 0.5) / ray_samples),
               -1000);
      Vec3 dir(0,0,1);
      Color col(0,0,0);   //  Resulting Color
      float coef = 1.0;  //  Reflection coefficient
      Ray ray(org,dir);   //  Create ray
      //  Loop up to maxlev times or until reflection coefficient becomes zero
      for (int level=0 ; level<maxlev && coef > 0 ; level++)
        {
          // Looking for intersection with the closest object
          int k = -1;
          // TODO: don't hardcode loop
          for (unsigned int i=0 ; i < numObjects ; i++) {
            int is_hit = 0;
            hit(&is_hit, &ray, &objects[i]);
            if (is_hit == 1) {
              k = i;
            }
          }
          if (k<0) {
            break;
          }  //  This ray left the scene

          //  Material type of this object
          Material mat = objects[k].mat;

          //  Point on current object where hit occurs
          Vec3 P = ray.org + ray.t*ray.dir;

          // Calculate new normal
          Vec3 N = normalize(P - objects[k].pos);

          //  Ambient light
          col += coef * 0.1 * mat.col;

          // Calculate color of pixel based on all the lights
          // TODO: don't hardcode loop
          for (unsigned int j=0 ; j < numLights ; j++)
            {
              //  L is the vector to the light
              Vec3 L = normalize(lights[j].pos - P);
              //  If N and L point in opposite directions, this is the shadow side
              if (N*L<=0) continue;
              //  Construct a new ray from hit point to light
              Ray ray(P , L);
              // Check if any object blocks the light
              int shadow = 0;
              for (unsigned int i=0 ; i < numObjects && !shadow; i++)
                hit(&shadow, &ray, &objects[i]);
              //  Lambertian (diffuse) reflection from object
              if (shadow == 0) col += coef * (L * N) * lights[j].col * mat.col;
            }

          //  Iterate to the next (specular) reflection
          coef *= mat.reflection;
          ray = Ray(P , normalize(ray.dir-2*(ray.dir*N)*N));
        }

      //  Return the resulting color
      avg_color += col;
    }
  }
  // char* pix = output_image + 4 * index;
  // *pix++ = // avg_color.r/ray_samples>1 ? 255 :
  //          // (unsigned int)(255*avg_color.r)/ray_samples;
  // *pix++ = // avg_color.g/ray_samples>1 ? 255 :
  //          // (unsigned int)(255*avg_color.g)/ray_samples;
  // *pix++ = avg_color.b/ray_samples>1 ? 255 :
  //          (unsigned int)(255*avg_color.b)/ray_samples;
  // *pix++ = 255;
}


/*
 *  Convenience routine to output raster text
 *  Use VARARGS to make this more flexible
 */
void Print(const char* format , ...)
{
  char    buf[LEN];
  char*   ch=buf;
  va_list args;
  //  Turn the parameters into a character string
  va_start(args,format);
  vsnprintf(buf,LEN,format,args);
  va_end(args);
  //  Display the characters one at a time at the current raster position
  while (*ch)
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,*ch++);
}

/*
 *  Ray trace a pixel
 */
// Color RayTrace(Vec3 org,Vec3 dir)
// {
//   Color col(0,0,0);   //  Resulting Color
//   float coef = 1.0;  //  Reflection coefficient

//   // Our output image
//   Ray ray(org,dir);   //  Create ray

//   //  Loop up to maxlev times or until reflection coefficient becomes zero
//   for (int level=0 ; level<maxlev && coef > 0 ; level++)
//   {
//     // Looking for intersection with the closest object
//     int k = -1;
//     for (unsigned int i=0 ; i<objects.size() ; i++)
//       if (objects[i]->hit(ray))
//         k = i;
//     if (k<0) break;  //  This ray left the scene

//     //  Material type of this object
//     Material mat = objects[k]->mat;

//     //  Point on current object where hit occurs
//     Vec3 P = ray.org + ray.t*ray.dir;

//     // Calculate new normal
//     Vec3 N = objects[k]->normal(P);

//     //  Ambient light
//     col += coef * 0.1 * mat.col;

//     // Calculate color of pixel based on all the lights
//     for (unsigned int j=0 ; j<lights.size() ; j++)
//     {
//       //  L is the vector to the light
//       Vec3 L = normalize(lights[j].pos - P);
//       //  If N and L point in opposite directions, this is the shadow side
//       if (N*L<=0) continue;
//       //  Construct a new ray from hit point to light
//       Ray ray(P , L);
//       // Check if any object blocks the light
//       bool shadow = false;
//       for (unsigned int i=0 ; i<objects.size() && !shadow; i++)
//         shadow = objects[i]->hit(ray);
//       //  Lambertian (diffuse) reflection from object
//       if (!shadow) col += coef * (L * N) * lights[j].col * mat.col;
//     }

//     //  Iterate to the next (specular) reflection
//     coef *= mat.reflection;
//     ray = Ray(P , normalize(ray.dir-2*(ray.dir*N)*N));
//   }

//   //  Return the resulting color
//   return col;
// }

/*
 *  Ray trace pixels
 *  bminortx: Modified ray tracing technique taken from
 *    "Ray Tracing from the Ground Up"
 */
// void RayTracePixel(int k)
// {
//   int i = k%wid;
//   int j = k/wid;
//   Color avg_color(0, 0, 0);
//   for (int m=0; m < ray_samples; ++m) {
//     for (int n=0; n < ray_samples; ++n) {
//       float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
//       // Cast the intial ray from (x,y,-1000) down z axis
//       Vec3 org(zoom*(i-wid/2 + (n * r) / ray_samples),
//                zoom*(j-hgt/2 + (m * r) / ray_samples),
//                -1000);
//       Vec3 dir(0,0,1);
//       avg_color += RayTrace(rot*org , rot*dir);
//     }
//   }
//   int index = j * wid + i;
//   char* pix = pixels+4*index;
//   *pix++ = avg_color.r/ray_samples>1 ? 255 :
//            (unsigned int)(255*avg_color.r)/ray_samples;
//   *pix++ = avg_color.g/ray_samples>1 ? 255 :
//            (unsigned int)(255*avg_color.g)/ray_samples;
//   *pix++ = avg_color.b/ray_samples>1 ? 255 :
//            (unsigned int)(255*avg_color.b)/ray_samples;
//   *pix++ = 255;
// }

/*
 *  OpenGL (GLUT) calls this routine to display the scene
 */
void display()
{
  double t = 0.001*glutGet(GLUT_ELAPSED_TIME);
  int size = 4 * wid * hgt;
  int numBytes = size * sizeof(char);

  // Threads and blocks for CUDA
  dim3 threadsPerBlock(32, 32);
  dim3 numBlocks(wid / threadsPerBlock.x,
                 hgt / threadsPerBlock.y);
  cudaMalloc((void **) &device_pixels, numBytes);
  cudaMemcpy(device_pixels, pixels, numBytes, cudaMemcpyHostToDevice);
  RaycastGPU <<<numBlocks, threadsPerBlock>>>(device_pixels,
                                              dev_objects, numObject,
                                              dev_lights, numLight, 
                                              ray_samples, zoom, wid,
                                              hgt, maxlev);
  cudaMemcpy(pixels, device_pixels, numBytes, cudaMemcpyDeviceToHost);
  // for (int i=0; i<size/20; i++) {
  //   std::cout << static_cast<int>(pixels[i]) << std::endl;
  // }

  //  Time ray tracing
  t = 0.001*glutGet(GLUT_ELAPSED_TIME) - t;
  //  Blit scene to screen
  glWindowPos2i(0,0);
  glDrawPixels(wid,hgt,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
  //  Display
  glWindowPos2i(5,5);
  Print("Size %dx%d Time %.3fs Angle %d,Samples: %d, %d Levels %d",
        wid,hgt,t,th,ray_samples,ph,maxlev);
  //  Flush
  glFlush();
  glutSwapBuffers();
}

/*
 *  Set rotation matrix
 */
void SetRot(void)
{
  double M[16];
  //  Initialize Modelview
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  //  Apply rotations
  glRotated(ph,1,0,0);
  glRotated(th,0,1,0);
  //  Copy matrix to row vectors
  glGetDoublev(GL_MODELVIEW_MATRIX,M);
  rot.x.x = M[0]; rot.x.y = M[4]; rot.x.z = M[8];
  rot.y.x = M[1]; rot.y.y = M[5]; rot.y.z = M[9];
  rot.z.x = M[2]; rot.z.y = M[6]; rot.z.z = M[10];
  //  Reset
  glLoadIdentity();
}

/*
 *  GLUT calls this routine when an arrow key is pressed
 */
void special(int key,int x,int y)
{
  //  Right arrow key - increase angle by 5 degrees
  if (key == GLUT_KEY_RIGHT)
    th += 5;
  //  Left arrow key - decrease angle by 5 degrees
  else if (key == GLUT_KEY_LEFT)
    th -= 5;
  //  Up arrow key - increase elevation by 5 degrees
  else if (key == GLUT_KEY_UP)
    ph += 5;
  //  Down arrow key - decrease elevation by 5 degrees
  else if (key == GLUT_KEY_DOWN)
    ph -= 5;
  //  Page Up key - increase zoom
  else if (key == GLUT_KEY_PAGE_UP)
    zoom *= 0.9;
  //  Page Down key - decrease zoom
  else if (key == GLUT_KEY_PAGE_DOWN)
    zoom *= 1.1;
  //  Keep angles to +/-360 degrees
  th %= 360;
  ph %= 360;
  SetRot();
  //  Tell GLUT it is necessary to redisplay the scene
  glutPostRedisplay();
}

/*
 *  GLUT calls this routine when a key is pressed
 */
void key(unsigned char ch,int x,int y)
{
  //  Exit on ESC
  if (ch == 27)
    exit(0);
  //  Reset view angle
  else if (ch == '0')
    th = ph = 0;
  //  Change object spacing
  else if (ch == '-' && dim>0.5)
    dim -= 0.1;
  else if (ch == '+')
    dim += 0.1;
  //  Change level
  else if (ch == 'N' && maxlev>1)
    maxlev--;
  else if (ch == 'n')
    maxlev++;
  //  Move light
  else if (ch == '[')
    lights[0].pos = rotmat(+5,0,1,0)*lights[0].pos;
  else if (ch == ']')
    lights[0].pos = rotmat(-5,0,1,0)*lights[0].pos;
  else if (ch == 'r') {
    ray_samples = std::max(1, ray_samples-1);
  } else if (ch == 'R') {
    ray_samples++;
  }
  //  Reposition objects
  objects[0].move(Vec3(-87*dim,  50*dim,0));
  objects[1].move(Vec3(+87*dim,  50*dim,0));
  objects[2].move(Vec3(   0   ,-100*dim,0));
  //  Update Angle
  SetRot();
  //  Tell GLUT it is necessary to redisplay the scene
  glutPostRedisplay();
}

/*
 *  GLUT calls this routine when the window is resized
 */
void reshape(int width,int height)
{
  //  Window dimensions
  wid = width;
  hgt = height;
  //  Allocate pixels size of window, for host and device
  int size = 4 * wid * hgt;
  delete pixels;
  if (device_pixels) {
    cudaFree(device_pixels);
  }
  pixels = new char [size];
  for (int i=0; i<size; i++) {
    pixels[i] = 200;
  }
  //  Set the viewport to the entire window
  glViewport(0,0, wid,hgt);
}

/*
 *  Start up GLUT and tell it what to do
 */
int main(int argc,char* argv[])
{
  //  Initialize GLUT and process user parameters
  glutInit(&argc,argv);
  //  Request double buffered, true Color window with Z buffering at 600x600
  glutInitWindowSize(640,480);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  //  Create the window
  glutCreateWindow("Ray Traced Spheres");
  //  Tell GLUT to call "display" when the scene should be drawn
  glutDisplayFunc(display);
  //  Tell GLUT to call "reshape" when the window is resized
  glutReshapeFunc(reshape);
  //  Tell GLUT to call "special" when an arrow key is pressed
  glutSpecialFunc(special);
  //  Tell GLUT to call "key" when a key is pressed
  glutKeyboardFunc(key);
  // Create lights
  lights[0] = Light(-320,0,  -100 , 1.0,1.0,1.0);
  lights[1] = Light(+320,0,-10000 , 0.6,0.7,1.0);
  // Create objects
  objects[0] = Sphere(Vec3(-87,  50,0) , 100.0 , Material(1,1,0 , 0.5));
  objects[1] = Sphere(Vec3(+87,  50,0) , 100.0 , Material(0,1,1 , 0.5));
  objects[2] = Sphere(Vec3(  0,-100,0) , 100.0 , Material(1,0,1 , 0.5));
  cudaMalloc((void **)&dev_objects, numObject * sizeof(Object));
  cudaMalloc((void **)&dev_lights, numLight * sizeof(Light));
  cudaMemcpy(dev_objects, objects, numObject * sizeof(Object), cudaMemcpyHostToDevice);
  cudaMemcpy(dev_lights, lights, numLight * sizeof(Light), cudaMemcpyHostToDevice);
  //  Initialize rotation matrix
  SetRot();
  //  Pass control to GLUT so it can interact with the user
  glutMainLoop();
  return 0;
}
