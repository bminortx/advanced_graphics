#ifndef RAYSPHERE_H
#define RAYSPHERE_H
#ifdef __CUDACC__
#define CHDM __host__ __device__
#else
#define CHDM
#endif

#include "RayObject.h"

class Sphere : public Object
{
public:
  //  Constructor
  CHDM Sphere(Vec3 p,float s,Material m):
    Object(p,m)
  {
    size = s;
  }
  //  Intersection
};

/*
 *  Check if the ray hits the Sphere at positive t less than current value of t
 */
__device__
inline void hit(int* is_hit, Ray* r, Object* object)
{
  const float eps = 0.01;
  Vec3 h = object->pos - r->org;
  float m = h*r->dir;
  float g = m*m - h*h + object->size*object->size;
  if (g<0) is_hit = 0;
  float t0 = m - sqrt(g);
  float t1 = m + sqrt(g);
  if (t0>eps && t0<r->t)
    {
      r->t = t0;
      *is_hit = 1;
    }
  else if (t1 > eps && t1 < r->t)
    {
      r->t = t1;
      *is_hit = 1;
    }
  else
    *is_hit = 0;
}

/*
 *  Check if the ray hits the Sphere at positive t less than current value of t
 */
__device__
inline void normal(Vec3* normal, Vec3 p, Object* object)
{
  *normal = normalize(p - object->pos);
}


#endif
