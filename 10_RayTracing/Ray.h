#ifndef RAY_H
#define RAY_H

#ifdef __CUDACC__
#define CHDM __host__ __device__
#else
#define CHDM
#endif

#include "Vec.h"

//
//  Ray type
//
class Ray
{
 public:
  Vec3 org;
  Vec3 dir;
  float t;
  CHDM Ray(Vec3 o,Vec3 d)
  {
    org = o;
    dir = d;
    t = 1e300;
  }
};

//
//  Material type
//
class Material
{
 public:
  Color col;
  float reflection;
  CHDM Material()
  {
    reflection = 0;
  }
  CHDM Material(float r,float g,float b,float f)
  {
    col = Color(r,g,b);
    reflection = f;
  }
};

//
//  Light type
//
class Light
{
 public:
  Vec3  pos;
  Color col;
  CHDM Light() {
    pos = Vec3(0, 0, 0);
    col = Color(0, 0, 0);
  }
  CHDM Light(float x,float y,float z , float r,float g,float b)
  {
    pos = Vec3(x,y,z);
    col = Color(r,g,b);
  }
};

#endif
