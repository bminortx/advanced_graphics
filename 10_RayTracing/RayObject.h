#ifndef RAYOBJECT_H
#define RAYOBJECT_H

#ifdef __CUDACC__
#define CHDM __host__ __device__
#else
#define CHDM
#endif

#include "Ray.h"
#include <cuda.h>
#include <cuda_runtime_api.h>

class Object
{
 public:
  Vec3 pos;
  Material mat;
  float size;
  //  Constructor
  CHDM Object() {
    pos = Vec3(0, 0, 0);
    mat = Material();
    size = 1;
  }
  CHDM Object(Vec3 p,Material m)
  {
    pos = p;
    mat = m;
    size = 1;
  }
  //  Move sphere
  CHDM void move(Vec3 p)
  {
    pos = p;
  }
};

#endif
