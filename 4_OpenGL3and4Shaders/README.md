# Assignment 4: OpenGL 3 and 4 Shaders #

Create a program that allows a scene to be viewed in 3D from any
direction under user control using OpenGL 3&4 style shaders.

Time taken to complete assignment: About a day, total. Creating
fundamental matrices was a big refresher. Also, had a hard time
parsing what was deprecated and what could be used in OpenGL 4,
leading to a lot of backtracking.

## How to Use ##

The second shader (m/M) is the keybinding you want; it switches to a
OpenGL 4-style program

Key bindings
- m/M : Cycle through shaders
- o/O : Cycle through objects
- p/P : Toggle between orthogonal & perspective projection
- s/S : Start/stop light
- -/+ : Change light elevation
- a : Toggle axes
- arrows : Change view angle
- PgDn/PgUp : Zoom in and out
- 0 : Reset view angle
- ESC : Exit

- - - - - -

## Original Assignment ##

	Assignment 4: OpenGL 3&4 Shaders

	Create a program that allows a scene to be viewed in 3D from any
	direction under user control using OpenGL 3&4 style shaders. This
	means your shaders must use the in/out style of passing variables, and
	you must pass the transformation matrices and similar variables to the
	shader yourself.

	Your shaders MUST implement lighting and textures.  At a minimum, you
	can add lighting and textures to Example 6 provided in class.  If you
	feel more ambitious, you could try and eliminate all use of the fixed
	pipeline and eliminate all use of deprecated features like
	glTranslate, gluLookAt, etc.

	If you try to implement your own transformations, make sure that I
	have all that is needed to compile your program.  If you use GLM,
	Ubuntu provides libglm-dev version 0.9.5, so you may assume that this
	version of libglm is provided by the system.  (GLM provides C++
	classes for vectors and matrices and manipulating them.)

	What to submit:

	1) One ZIP or TAR archive containing all files you are submitting;

	2) Source code (shader and program), Makefile and any data files I
	need;

	3) README with brief instructions on how to use the program;

	4) Time it took you to complete the assignment.
